import React, { Component } from 'react'
import { getDataFromService } from './utils.js'
import $ from 'jquery'

class Homepage extends Component {

    constructor() {
        super();

        this.state = {
            chosenTestId: null,
            quizzes: []
        }

        this.chosenTestId = null
    }

    componentDidMount = () => {

        // Get quizzes from REST API
        getDataFromService('quizzes')
            .then(data => {
                this.setState({
                    quizzes: data
                });
            })
    }

    // event for selecting quiz from dropdown
    getSelectedValue = e => {
        this.chosenTestId = +e.target.value
    }

    startTest = () => {

        let errors = []

        this.userName = $('#userName').val()

        if (!this.userName.length)
            errors.push('Name')

        if (!this.chosenTestId)
            errors.push('Test')

        // If one of required fields are empty then showing error
        if (errors.length) {

            $('#validationError').html('Please fill empty fields - <b>' + errors.toString() + '</b>')
            this.showError()

            return
        }

        this.hideError()

        // Calling parent function to update homepage screen data in pageHandler
        this.props.setHomepageData({
            chosenTestId: this.chosenTestId,
            userName: this.userName
        })
    }

    // Functions to show or hide validation errors
    showError = () => $('#validationError').show()
    hideError = () => $('#validationError').hide()

    render() {

        let { quizzes } = this.state
        let { userName } = this.props

        return (
            <React.Fragment>
                <div className="row justify-content-md-center">
                    <div id="validationError" className="alert alert-danger col col-lg-6 col-sm-12" style={{ display: "none" }} role="alert"></div>
                </div>
                <br />
                <div className="row justify-content-md-center">
                    <div className="col col-lg-6 col-sm-12">
                        <div className="input-group mb-3 errorLevelName">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="inputGroup-sizing-default"><b>Name</b></span>
                            </div>
                            <input id='userName' type="text" className="form-control" aria-label="Sizing example input"
                                aria-describedby="inputGroup-sizing-default" defaultValue={userName}>
                            </input>
                        </div>
                    </div>
                </div>

                <div className="row justify-content-md-center">
                    <div className="col col-lg-6 col-sm-12">
                        <div className="input-group mb-3 errorLevelTest">
                            <div className="input-group-prepend">
                                <span className="input-group-text" id="inputGroup-sizing-default"><b>Test</b></span>
                            </div>
                            <select className="form-control" id="exampleFormControlSelect1" onChange={this.getSelectedValue}>
                                <option key="0" value="">Select test</option>
                                {quizzes.map(quizz => {
                                    return <option key={quizz.id} value={quizz.id}>{quizz.title}</option>
                                })}
                            </select>
                            <br />
                        </div>
                    </div>
                </div>

                <br />
                <div className="row justify-content-md-center">
                    <div className="col col-lg-6 col-sm-12 textAlignCenter">
                        <button type="button" className="btn btn-dark btn-lg" onClick={this.startTest}>Start test</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Homepage;