import React, { Component } from 'react'
import { getDataFromService } from './utils.js'

class Results extends Component {

    constructor() {
        super();

        this.state = {
            results: []
        }
    }

    componentDidMount = () => {
        this.getResults()
    }

    getResults = () => {

        let {answers, chosenTestId} = this.props
        let answersUrl = ''

        answersUrl = answersUrl + answers.map(answer => {
            return '&answers[]=' + answer.chosenAnswer
        })

        // Get results from REST API
        getDataFromService('submit&quizId=' + chosenTestId + answersUrl)
            .then(data => {
                this.setState({
                    results: data
                });
            })
    }

    render() {

        let { results } = this.state
        let { userName } = this.props

        console.log('render results', results)

        if (!results.length) {
            return <div></div>
        }

        return (
            <React.Fragment>
                <br/>
                <div className="row">
                    <div className="col col-lg-12 textAlignCenter">
                        <h3>Hey, {userName}!</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col col-lg-12 textAlignCenter">
                        <h3>You answered correctly {results[0].correct} of {results[0].total} questions.</h3>
                    </div>
                </div>
                <br/>
                <div className="row justify-content-md-center">
                    <div className="col col-lg-6 col-sm-12 textAlignCenter">
                        <button type="button" className="btn btn-danger btn-lg" onClick={() => this.props.setHomepageData({userName: userName})}>Start over</button>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Results;