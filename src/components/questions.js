import React, { Component } from 'react'
import { getDataFromService } from './utils.js'

class Questions extends Component {

    constructor() {
        super();

        this.state = {
            questions: [],
            index: 0
        }

        this.progress = 0
        this.progressStep = 0
    }

    componentDidMount = () => {
        this.getQuestions(this.props.chosenTestId)
    }

    componentDidUpdate = () => {

        let { questions, index } = this.state

        if (questions[index]['answers'] && questions[index]['answers'].length) {
            return
        }

        if (!questions[index]) {
            return;
        }

        this.getAnswers(questions[index])
    }

    getQuestions = chosenTestId => {

        // Get questions from REST API
        getDataFromService('questions&quizId=' + chosenTestId)
            .then(data => {
                this.progressStep = 100 / data.length
                this.progress = this.progressStep
                this.setState({
                    questions: data
                });
            })
    }

    getAnswers = question => {

        let questions = this.state.questions

        // Get questions from REST API
        getDataFromService('answers&quizId=' + this.props.chosenTestId + '&questionId=' + question.id)
            .then(data => {

                questions.map((questionFromList, index) => {

                    if (questionFromList.id === question.id) {
                        questionFromList['answers'] = data
                    }

                    return questionFromList
                })

                this.setState({ questions: questions })
            })
    }

    setAnswer = answerId => {

        if (!answerId) {
            return
        }

        let { questions, index } = this.state
        let nextIndex = index + 1

        this.progress = this.progress + this.progressStep
        questions[index]['chosenAnswer'] = answerId

        if (!questions[nextIndex]) {
            this.props.setAnswers(questions)
            return
        }

        this.setState({
            index: nextIndex,
            questions: questions
        })
    }

    render() {

        let { questions, index } = this.state

        if (!questions.length) {
            return <div></div>
        }

        // Showing loader while answers are fetching from Rest API
        if (!questions[index]['answers']) {
            return <div className="row">
                <div className="col col-lg-12 col col-sm-12 textAlignCenter center-loader">
                    <div className="spinner-border text-primary" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        }

        return (
            <React.Fragment>
                <br/>
                <div className="row">
                    <div className="col col-lg-12 col col-sm-12">
                        <div className="textAlignCenter"><h2>{questions[index].title}</h2></div>
                    </div>
                </div>
                <br/>
                <div className="row">{questions[index].answers.map(answer => {
                    return <div key={answer.id} className="col col-lg-6 col-sm-12 col-12 textAlignCenter">
                        <button type="button" className="btn btn-primary button-margin col col-lg-6 col-sm-12 col-12" onClick={() => { this.setAnswer(answer.id) }}>{answer.title}</button>
                    </div>
                })}</div>
                <br/>
                <div className="row justify-content-lg-center">
                    <div className="col-lg-10 col-sm-10 col-10 textAlignCenter">
                        <div className="progress">
                            <div className="progress-bar" role="progressbar" style={{width: this.progress + "%"}} aria-valuenow={this.progress}
                                aria-valuemin="0" aria-valuemax="100">{Number.parseFloat(this.progress).toFixed(0)}%</div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Questions;