import React, { Component } from 'react'
import Homepage from './homepage'
import Questions from './questions'
import Results from './results'

class PageHandler extends Component {

    constructor() {
        super();

        this.state = {
            userName: '',
            chosenTestId: null,
            questions: [],
            answers: []
        }
    }

    // Function are called from child component to update data in pageHandler
    setHomepageData = params => {

        let {chosenTestId, userName} = params

        this.setState({
            userName: userName,
            chosenTestId: chosenTestId,
            questions: [],
            answers: []
        })
    }

    // Update this.state with answers (from API call)
    setAnswers = answers => this.setState({answers: answers})

    render() {

        let { chosenTestId, answers } = this.state
        let contentJSX

        // Showing one of three screens depending from loaded data in this.state
        if (answers.length) {
            contentJSX = <Results userName={this.state.userName} chosenTestId={this.state.chosenTestId}
                answers={answers} setHomepageData={this.setHomepageData}/>
        } else if (chosenTestId) {
            contentJSX = <Questions chosenTestId={this.state.chosenTestId} setAnswers={this.setAnswers}/>
        } else {
            contentJSX = <Homepage userName={this.state.userName} setHomepageData={this.setHomepageData}/>
        }

        return (
            <div className="container center-screen">
                {contentJSX}
            </div>
        )
    }
}

export default PageHandler;