import React from 'react';
import PageHandler from './components/pageHandler';
import './App.css';

function App() {
  return (
    <div className="container">
      <PageHandler/>
    </div>
  );
}

export default App;
