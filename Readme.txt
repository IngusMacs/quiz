This is React.JS project. To run project please follow these steps:
1. install Node.JS (project developed in version 12.14.1 LTS)
2. in terminal in source dir run "npm install"
3. and then "npm start"

Project is using axios driver for Rest API calls, JQuery for DOM manipulations and Bootstrap 4 styles and animations.
